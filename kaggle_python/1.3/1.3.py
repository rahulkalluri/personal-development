# here we're passing a default value into the function
# can be changed
def greet(who="Colin"):
    print("Hello,", who)

# an example of triple quotes which give you help() text
def call(fn, arg):
    """Call fn on arg"""
    return fn(arg)

# throwaway functions are called lambda functions
mod_5 = lambda x: x % 5

