#query 1
select * from EMPLOYEES where ADDRESS LIKE '%Elgin,IL%';

#query 2
select * from EMPLOYEES where B_DATE LIKE '197%'

#query 3
select F_NAME, L_NAME, SALARY from EMPLOYEES WHERE SALARY BETWEEN 60000 and 70000;

#query 4
select D.DEP_NAME, E.F_NAME, E.L_NAME 
from EMPLOYEES as E, DEPARTMENTS as D
where E.DEP_ID = D.DEPT_ID_DEP
order by D.DEP_NAME, E.L_NAME desc;

#query 5
SELECT DEP_ID, AVG(SALARY) as AVG_SALARY, COUNT(*) FROM EMPLOYEES
group by DEP_ID
