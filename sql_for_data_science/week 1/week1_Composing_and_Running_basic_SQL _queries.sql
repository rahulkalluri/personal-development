#task 0
drop instructor if exists; 

#task 1
create table instructor 
  (ins_id  integer primary key not null,
  lastname  varchar(100) not null,
  firstname  varchar(100) not null,
  city varchar(100) ,
  country  char(2) 
)

#task 2A
insert into instructor
  (ins_id, lastname, firstname, city, country)
  VALUES (1, 'Ahuja', 'Rav', 'Toronto', 'CA')
  
#task 2B
insert into instructor
  (ins_id, lastname, firstname, city, country)
  values (2, 'Chong', 'Raul', 'Toronto', 'CA'),
  		 (3, 'Vasudevan', 'Hima', 'Chicago', 'US')
  		 
#task 3
select * from instructor

#task 3B
select firstname, lastname, country from instructor where city = 'Toronto'

#task 4
Update instructor 
set city = 'Markham'
where ins_id = 1

#task 5
delete from instructor where ins_id = 2

#task 5b
select * from instructor